<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('v2/expdte-iniciarsesion');
});

Route::get('registro', function(){
    return view('v2/expdte-registrousuarios');
});

Route::post('inicio',function(){
    return view('expdte-vistausuarios');
});

Route::get('registropaciente', function(){
    return view('v2/expdte-registropaciente');
});

Route::get('expedienteclinico', function(){
    return view('v2/expdte-expedienteclinico');
});

Route::get('informacionpaciente', function(){
    return view('v2/expdte-informacionpaciente');
});