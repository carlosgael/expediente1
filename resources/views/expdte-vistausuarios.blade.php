<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
            crossorigin="anonymous">
    <link rel="stylesheet" href="{!! asset('css/stylesheet.css') !!}"> 

    <title>Bienvenido | Expediente clínico</title>
</head>
    <body>

    <!--nav1-->
    <nav class="navbar navbar-expand-lg navbar-dark shadow-sm" style="background-color: #32dc32;">
        <span class="navbar-brand mb-0 h1">Expediente clínico</span>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" style="color: white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Pacientes
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ url('registropaciente') }}"><span class="fas fa-user-plus" style="margin-right: 0.4rem"></span>Registrar paciente</a>
                    <a class="dropdown-item" href="{{ url('informacionpaciente') }}"><span class="fas fa-user-plus" style="margin-right: 0.4rem"></span>Ejemplo info. paciente</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" style="color: white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Agenda
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#"><span class="fas fa-calendar-plus" style="margin-right: 0.4rem"></span>Agendar cita</a>
                    <a class="dropdown-item" href="#"><span class="fas fa-calendar-alt" style="margin-right: 0.4rem"></span>Ver agenda</a>
                </div>
            </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <a class="btn btn-link my-2 my-sm-0" href="#" style="padding: 0em">Cerrar sesión</a>
            </form>
    </div>
    </nav>
    <!--nav-->

        <!--Buscador--> 
        <div class="main-content">
            <form class="form-inline justify-content-center"  style=" width: 100vw;" action="">
                <input class="form-control mr-sm-2 shadow-sm"
                    name="curp"
                    maxlength="18"
                    style="width: 60vw; -webkit-border-radius: 50px 0px 0px 50px; -moz-border-radius: 50px 0px 0px 50px; border-radius: 50px 0px 0px 50px;" 
                    type="search" 
                    placeholder="Ingrese folio, curp o nombre del paciente..." 
                    aria-label="Search"
                    autocomplete="off">
                <button class="btn button_hov my-2 my-sm-0 shadow-sm" 
                        type="submit" 
                        style=" -webkit-border-radius: 0px 50px 50px 0px; -moz-border-radius: 0px 50px 50px 0px; border-radius: 0px 50px 50px 0px;" 
                        title="Buscar"><span class="fas fa-search"></span></button>
            </form>
        </div> 
        <!---->

        <!--<a href="{{ url('informacionpaciente') }}" class="btn btn-success" role="button">Información paciente</a>-->
        
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    
    </body>
</html>