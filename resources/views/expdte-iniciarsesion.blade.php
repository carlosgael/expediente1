<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
          crossorigin="anonymous">

    <link rel="stylesheet" href="{!! asset('css/stylesheet.css') !!}">      

    <title>Iniciar Sesión | Expediente Clínico</title>
</head>
<body>
  
    <div class= "main-content">
        <div class="container">
            <div class="card shadow-sm" style="width: 20rem; border-radius: 10px">
                <div class="card-header" style=" background-color: #32dc32; color: #FFFFFF; font-weight: bold">
                    Iniciar Sesión
                </div>
                <div class="card-body">
                    <form action="{{ url('inicio') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="email">Correo electrónico</label>
                            <input type="email" class="form-control" id="email" name="correo" aria-describedby="Ingrese correo electrónico válido" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" id="password" name="contraseña" aria-describedby="Ingrese su contraseña" required>
                        </div>
                        <div style="text-align: center">
                            <button type="submit" class="btn button_hov" style="margin-top: 0.5em; margin-bottom: 3em; width:100%">Ingresar</button>
                        </div>
                    </form>
                    <div class="link-content"> 
                        <a href="registro" class="card-link">Registrarme</a>
                        <a href="#" class="card-link">Recuperar contraseña</a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>