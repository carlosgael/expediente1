<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <!--Font awesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
            crossorigin="anonymous">
        <link rel="stylesheet" href="{!! asset('css/stylesheet.css') !!}"> 

        <title>Información paciente | Expediente clínico</title>
    </head>

    <body>
        <!--inicio-Contenedor principal-->
        <div class="container" style="margin-top: 2.5rem">
            <!--/////////////////////////////////////////-->
            <div class="card" style="width: 80vw; border-color: transparent; background-color: transparent">
                <div class="card-body" style="border-color: White;">
                    <div class="accordion shadow"  style="border-color: #2d90c2; border-radius: 10px 10px 10px 10px;" id="accordionExample">
                        <div class="card" style="border-color: inherit; border-radius: 10px 10px 0px 0px">
                            <div class="card-header" style="padding: 0em; background-color: #33a6e0" id="headingFour">
                                <h2 class="mb-0">
                                    <button class="btn btn-block collapsed" style="color: White; text-align: left" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Información de Registro
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">
                                    <!---->
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="formGroupInput">No. de Paciente</label>
                                                <input type="text" class="form-control" id="formGroupInput" placeholder="" value="1111111" pattern="[A-Za-z]+" disabled required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="formGroupInput2">Folio de identificación</label>
                                                <input type="text" class="form-control" id="formGroupInput2" placeholder="" pattern="[A-Za-z]+" disabled required>
                                            </div>
                                        </div>  
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group"> 
                                                <label for="userdate">Fecha de registro</label>
                                                <input type="date" class="form-control" id="userdate" name="date" disabled required>
                                            </div>
                                        </div>
                                    </div>
                                    <!---->   
                                </div>
                            </div>
                        </div>
                        <div class="card" style="border-color: inherit;">
                            <div class="card-header" style="padding: 0em; background-color: #33a6e0" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-block" style="color: White; text-align: left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Datos Generales
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <!---->
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12">
                                            <div class="form-group">
                                                <label for="formGroupInput">Nombre(s)</label>
                                                <input type="text" class="form-control" id="formGroupInput" placeholder="" pattern="[A-Za-z]+" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="formGroupInput2">Apellido Paterno</label>
                                                <input type="text" class="form-control" id="formGroupInput2" placeholder="" pattern="[A-Za-z]+" required>
                                            </div>
                                        </div>  
                                        <div class="col-lg-4 col-md-6">      
                                            <div class="form-group">
                                                <label for="formGroupInput3">Apellido Materno</label>
                                                <input type="text" class="form-control" id="formGroupInput3" placeholder="" pattern="[A-Za-z]+" required>
                                            </div>
                                        </div>   
                                    </div>
                                    <!---->  
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group"> 
                                                <label for="userdate">Fecha de nacimiento</label>
                                                <input type="date" class="form-control" id="userdate" name="date" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="FormControlSelect3">Sexo</label>
                                                <select class="form-control" id="FormControlSelect3">
                                                    <option value="0">Seleccione una opción...</option>
                                                    <option value="1">Masculino</option>
                                                    <option value="2">Femenino</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="FormControlSelect3">Estado civil</label>
                                                <select class="form-control" id="FormControlSelect3">
                                                    <option value="0">Seleccione una opción...</option>
                                                    <option value="1">Casado</option>
                                                    <option value="2">Soltero</option>
                                                    <option value="3">Divorciado</option>
                                                    <option value="4">Otro</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">      
                                            <div class="form-group">
                                                <label for="formGroupInput3">CURP</label>
                                                <input type="text" class="form-control" id="formGroupInput3" placeholder="" pattern="" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="FormControlSelect3">Nacionalidad</label>
                                                <select class="form-control" id="FormControlSelect3">
                                                    <option value="0">Seleccione una opción...</option>
                                                    <option value="1">Mexicana</option>
                                                    <option value="2">Extranjera</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="FormControlSelect3">Estado de nacimiento</label>
                                                <select class="form-control" id="FormControlSelect3">
                                                    <option value="0">Seleccione una opción...</option>
                                                    <option value="1">Aguascalientes</option>
                                                    <option value="2">Baja California</option>
                                                    <option value="3">Baja California Sur</option>
                                                    <option value="4">Campeche</option>
                                                    <option value="5">Chiapas</option>
                                                    <option value="6">Chihuahua</option>
                                                    <option value="7">Ciudad de México</option>
                                                    <option value="8">Coahuila</option>
                                                    <option value="9">Colima</option>
                                                    <option value="10">Durango</option>
                                                    <option value="11">Estado de México</option>
                                                    <option value="12">Guanajuato</option>
                                                    <option value="13">Guerrero</option>
                                                    <option value="14">Hidalgo</option>
                                                    <option value="15">Jalisco</option>
                                                    <option value="16">Michoacán</option>
                                                    <option value="17">Morelos</option>
                                                    <option value="18">Nayarit</option>
                                                    <option value="19">Nuevo León</option>
                                                    <option value="20">Oaxaca</option>
                                                    <option value="21">Puebla</option>
                                                    <option value="22">Querétaro</option>
                                                    <option value="23">Quintana Roo</option>
                                                    <option value="24">San Luis Potosí</option>
                                                    <option value="25">Sinaloa</option>
                                                    <option value="26">Sonora</option>
                                                    <option value="27">Tabasco</option>
                                                    <option value="28">Tamaulipas</option>
                                                    <option value="29">Tlaxcala</option>
                                                    <option value="30">Veracruz</option>
                                                    <option value="31">Yucatán</option>
                                                    <option value="32">Zacatecas</option>
                                                </select>
                                            </div>
                                        </div> 
                                    </div>      
                                </div>
                            </div>
                        </div>
                        <div class="card" style="border-color: inherit;">
                            <div class="card-header" style="padding: 0em; background-color: #33a6e0" id="headingTwo">
                                <h2 class="mb-0">
                                    <button class="btn btn-block collapsed" style="color: White; text-align: left" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Contacto
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="formGroupInput4">Teléfono</label>
                                                <input type="tel" class="form-control" id="formGroupInput4" placeholder="" pattern="[0-9]+">
                                            </div>  
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="formGroupInput5">Celular(Móvil)</label>
                                                <input type="tel" class="form-control" id="formGroupInput5" placeholder="" pattern="[0-9]+">
                                            </div>  
                                        </div>
                                        <div class="col-lg-4 col-md-6">
                                            <div class="form-group">
                                                <label for="FormControlInput1">Correo Electrónico</label>
                                                <input type="email" class="form-control" id="FormControlInput1" placeholder="correo@ejemplo.com" required>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="card" style="border-color: inherit;">
                            <div class="card-header" style="padding: 0em; background-color: #33a6e0" id="headingFive">
                                <h2 class="mb-0">
                                    <button class="btn btn-block collapsed" style="color: White; text-align: left" type="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        Información Adicional
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionExample">
                                <div class="card-body">
                                    <!---->
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6">      
                                            <div class="form-group">
                                                <label for="formGroupInput3">Religión</label>
                                                <input type="text" class="form-control" id="formGroupInput3" placeholder="" pattern="" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">      
                                            <div class="form-group">
                                                <label for="formGroupInput3">Ocupación</label>
                                                <input type="text" class="form-control" id="formGroupInput3" placeholder="" pattern="" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-6">      
                                            <div class="form-group">
                                                <label for="formGroupInput3">Lengua indígena</label>
                                                <input type="text" class="form-control" id="formGroupInput3" placeholder="" pattern="" required>
                                            </div>
                                        </div>
                                    </div>  
                                    <!---->   
                                </div>
                            </div>
                        </div>
                        <div class="card" style="border-color: inherit; border-radius: 0px 0px 10px 10px">
                            <div class="card-header" style="padding: 0em; background-color: #33a6e0" id="headingThree">
                                <h2 class="mb-0">
                                    <button class="btn btn-block collapsed" style="color: White; text-align: left" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Información Médica
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="col-lg-4 col-md-6">      
                                        <div class="form-group">
                                            <label for="formGroupInput3">Tipo de sangre</label>
                                            <input type="text" class="form-control" id="formGroupInput3" placeholder="" pattern="" required>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/////////////////////////////////////////-->
        </div>
        <!--fin-Contenedor principal-->

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

    </body>
</html>