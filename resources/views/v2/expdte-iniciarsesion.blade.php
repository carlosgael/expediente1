<!DOCTYPE html>
<html lang="es">
<head>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--Font awesome-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
          crossorigin="anonymous">

    <link rel="stylesheet" href="{!! asset('css/stylesheet.css') !!}">      

    <title>Iniciar Sesión | Expediente Clínico</title>
</head>
<body>
  
    <div class= "main-content">
        <div class="container">
            <div class="row shadow" style="width: 45rem; border-radius: 10px 10px 10px 10px">
                <div class="col-lg-6" style="padding: 0em;  border-radius: 10px 0px 0px 10px">
                    <div class="card" style="border-radius: inherit; border-color: White ">
                        <div class="card-header" style="border-color:White; border-radius: 10px; background-color: White; color: Gray; font-weight: bold">
                            LOGO
                        </div>
                        <div class="card-body" >
                            <form action="{{ url('inicio') }}" method="POST">
                                {{ csrf_field() }}
                                <div style="color: Gray; font-weight: bold;">
                                    INICIAR SESIÓN
                                </div>
                                <P style="color: Gray;">Ingrese sus datos de acceso para continuar</P>

                                <div class="input-group mb-3" style="margin-top: 1.5rem">
                                    <div class="input-group-append">
                                        <span class="input-group-text" style="color: Gray; background-color: White; border-color: LightGray White LightGray LightGray; border-radius: 5px 0px 0px 5px "><i class="fas fa-user"></i></span>
                                    </div>
                                    <input type="text" name="" class="form-control" value="" placeholder="Usuario" required>
                                </div>
                                
                                <div class="input-group mb-2">
                                    <div class="input-group-append">
                                        <span class="input-group-text" style="color: Gray; background-color: White; border-color: LightGray White LightGray LightGray; border-radius: 5px 0px 0px 5px "><i class="fas fa-key"></i></span>
                                    </div>
                                    <input type="password" name="" class="form-control" value="" placeholder="Contraseña" required>
                                </div>

                                <div style="text-align: center">
                                    <button type="submit" class="btn button_hov" style="margin-top: 0.5em; margin-bottom: 3em; width:100%">Iniciar Sesión</button>
                                </div>
                            </form>
                            <div class="link-content" style="margin-top: 0.5em"> 
                                <a href="registro" class="card-link">Pre-Registro</a>
                                <a href="#" class="card-link">Olvidó su contraseña?</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6" style="padding: 1em;
                                             border-radius: 0px 10px 10px 0px;   
                                             background-color: #33a6e0; 
                                             display: flex; 
                                             align-items: center;">   
                    <div>
                        <img style="width: 100%" src="{!! asset('img/undraw_medicine_b1ol.svg') !!}" class="" alt="">      
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>
</html>